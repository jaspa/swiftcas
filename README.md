# SwiftCAS #

_SwiftCAS_ is a try to implement something like a [*Computer Algebra System* (CAS)](https://en.wikipedia.org/wiki/Computer_algebra_system) using Swift.

But as I am not a scientist on this topic, the whole implementation is done using my personal knowledge of maths und algorithms.

## Built-In reductions ##

| Reduction         | Result | Reductor name             | Backwards support |
|-------------------|--------|---------------------------|-------------------|
| `a + 0`           | `a`    | ZeroReductor              | no                |
| `a * 0`           | `0`    | ZeroReductor              | no                |
| `a + (-a)`        | `0`    | IdentitySubstractReductor | no                |
| `a * 1`           | `a`    | OneReductor               | no                |
| `a * (-1)`        | `-a`   | OneReductor               | no                |
| `c1 + c2`         | …      | SimpleConstantReductor    | no                |
| `c1 * c2`         | …      | SimpleConstantReductor    | no                |
| `.Neg(.Const(c))` | `-c`   | NegationReductor          | yes               |
