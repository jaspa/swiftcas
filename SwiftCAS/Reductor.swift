//
//  Reductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public protocol Reductor: ReductorType {

    // Repeat the declarations, so things like
    //   struct SomeReductor<Constant: Value, Hole: Variable>: Reductor { ... }
    // work
    typealias Constant: Value
    typealias Hole: Variable

    /// Returns `true` iff the reductor's `runOnTerm` function wants to be invoked upon `.Hole` and `.Const`
    ///
    /// The default implementation returns `false`.
    var wantsLeafs: Bool { get }

    /// - warning: Don't invoke this function directly but use `Term.reduce(_:)`.
    /// - note: You don't have to recurse into the given term yourself. This is handled by the calling function.
    /// - returns: Returns the modified term or `nil` if there was no modification.
    func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>?
}

public extension Reductor {
    var wantsLeafs: Bool { return false }

    func reduce(var term: Term<Constant, Hole>) -> ReductorResult<Constant, Hole> {
        let operands = term.operands

        if operands.isEmpty {
            return wantsLeafs ? ReductorResult(newTerm: runOnTerm(term), fallback: term) : ReductorResult(term: term)
        }

        term.operands = operands.map(reduceToTerm)
        return ReductorResult(newTerm: runOnTerm(term), fallback: term)
    }
}
