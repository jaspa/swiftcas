//
//  Value.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public enum Sign {
    case Positive, Zero, Negative
}

public protocol Value: Comparable, Hashable {
    /// Initialize `Self` to zero.
    ///
    /// **Axioms:**
    /// - `Self() == Self(doubleValue: 0.0)`
    ///
    /// The default implementation calls `init(doubleValue: 0.0)`. A faster version may be provided.
    init()

    /// Initialize `Self` to the given `value`.
    init(doubleValue value: Double)

    /// Get the value representing 1.0
    static var one: Self { get }

    var isZero: Bool { get }
    var isOne: Bool { get }

    // Return whether the number is greater than, equal to or less than zero.
    var sign: Sign { get }

    var negated: Self { get }
    func add(value: Self) -> Self
    func mul(value: Self) -> Self
    func increment() -> Self
}

public extension Value {
    init() { self.init(doubleValue: 0.0) }
    static var one: Self { return Self(doubleValue: 1.0) }

    var isZero: Bool { return self == Self() }
    var isOne: Bool { return self == Self(doubleValue: 1.0) }

    var sign: Sign { return self.isZero ? .Zero : self > Self() ? .Positive : .Negative }

    var negated: Self { return mul(Self(doubleValue: -1.0)) }
    func increment() -> Self { return add(Self(doubleValue: 1.0)) }
}

postfix func ++<V: Value>(inout v: V) -> V {
    v = v.increment()
    return v
}
