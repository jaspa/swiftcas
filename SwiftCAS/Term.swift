//
//  Term.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public indirect enum Term<C: Value, H: Variable> {
    case Hole(H)
    case Const(C)
    case Neg(Term)
    case Sum(Term, Term)
    case Prod(Term, Term)

    public static var One: Term { return .Const(.one) }
    public static var Zero: Term { return .Const(C()) }
}

extension Term: Equatable {
    public func isEqualToTerm(other: Term, relaxOrdering: Bool = true) -> Bool {
        func checkSubterms(aa: (Term, Term), _ bb: (Term, Term)) -> Bool {
            return aa == bb || (relaxOrdering && swap(aa) == bb)
        }

        switch (self, other) {
            case let (.Hole(a) , .Hole(b) ): return a == b
            case let (.Const(a), .Const(b)): return a == b
            case let (.Neg(a)  , .Neg(b)  ): return a == b
            case let (.Sum(aa) , .Sum(bb) ): return checkSubterms(aa, bb)
            case let (.Prod(aa), .Prod(bb)): return checkSubterms(aa, bb)
            default: return false
        }
    }
}

private func ==<A: Equatable, B: Equatable>(a: (A, B), b: (A, B)) -> Bool { return a.0 == b.0 && a.1 == b.1 }
private func swap<A, B>(t: (A, B)) -> (B, A) { return (t.1, t.0) }

/// Two Terms are considered to be equal, if their trees are equal but not if their folded values are equal.
///
///     .Sum(.Const(1), .Const(2)) == .Sum(.Const(2), .Const(1))
///     .Sum(.Const(1), .Const(2)) != .Const(3)
public func ==<C: Value, H: Variable>(lhs: Term<C, H>, rhs: Term<C, H>) -> Bool {
    return lhs.isEqualToTerm(rhs)
}

extension Term: Hashable {
    // Feel free to improve this implementation!
    public var hashValue: Int {
        switch self {
            case let .Hole(h)   : return h.hashValue
            case let .Const(c)  : return c.hashValue
            case let .Neg(t)    : return t.hashValue               ^ 0x723c143b
            case let .Sum(a, b) : return a.hashValue ^ b.hashValue ^ 0x9e3779b9
            case let .Prod(a, b): return a.hashValue ^ b.hashValue ^ 0x2db663af1
        }
    }
}
