//
//  NegationReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 05.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public enum NegationMode {
    case Reducing, Decomposing
}

/// Eliminate `.Neg(.Const)` using NegationReductor.
///
/// NegationReductor has two modes:
///     
///     .Const(-c)      ==> .Neg(.Const(c))  /* .Decomposing */
///     .Neg(.Const(c)) ==> .Const(-c)       /* .Reducing    */
///
/// In both modes a double negation is removed:
///
///     .Neg(.Neg(x)) ==> x
public struct NegationReductor<Constant: Value, Hole: Variable>: Reductor {

    /// The mode in which this reductor operates
    public let mode: NegationMode

    public init(mode: NegationMode) {
        self.mode = mode
    }

    private var isDecomposing: Bool { return mode == .Decomposing }

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        if isDecomposing {
            if case let .Const(c) = term where isDecomposing && c.sign == Sign.Negative {
                return .Neg(.Const(c.negated))
            }
        } else if case let .Neg(.Const(c)) = term {
            return .Const(c.negated)
        }

        if case let .Neg(.Neg(x)) = term {
            return x
        }

        return nil
    }

    public var wantsLeafs: Bool { return isDecomposing }
}
