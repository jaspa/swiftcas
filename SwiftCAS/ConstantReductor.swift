//
//  ConstantReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// Reduce “simple” constant operations. This includes operations not producing an infinite number of decimal places.
/// The only exception is the negation which is handled by the `NegationReductor`.
///
/// - note: `NegationReductor` should be run first in `.Reduction` mode.
public struct SimpleConstantReductor<Constant: Value, Hole: Variable>: Reductor {

    public init() {}

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        if case let .Sum(.Const(a), .Const(b)) = term { return .Const(a.add(b)) }
        if case let .Prod(.Const(a), .Const(b)) = term { return .Const(a.mul(b)) }
        return term
    }
}

/// A `ConstantFolder` is a machine which can fold multiple values into one according to some specific type of operation.
public protocol ConstantFolder {

    typealias Constant: Value

    /// Fold the given constants into one with an operation according to `operation`.
    ///
    /// - returns: Returns the folded value or `nil` if this type of operation/these values are not supported.
    func foldConstants(values: [Constant], withOperation operation: OperationType) -> Constant?
}

/// Fold constant values using a given ConstantFolder.
///
/// - note: `NegationReductor` should be run first in `.Reduction` mode except it is handled in the provided folder.
public struct ConstantReductor<Folder: ConstantFolder, Hole: Variable>: Reductor {
    public typealias Constant = Folder.Constant

    private let folder: Folder
    private let simpleFolder: SimpleConstantReductor<Constant, Hole>?

    /// Initialize a ConstantReductor with the given `folder`
    ///
    /// - parameter folder: The folder with which the more complex expressions (div/roots/...) are folded.
    /// - parameter useSimpleReductor: If this is `true` (the default value) an instance of `SimpleConstantReductor` 
    ///   first tries to reduce the given term, and if that fails it is passed to the given `folder`.
    public init(folder: Folder, useSimpleReductor: Bool = true) {
        self.folder = folder
        self.simpleFolder = useSimpleReductor ? SimpleConstantReductor() : nil
    }

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        // handle the simple cases if wanted
        if let simpleModified = simpleFolder?.runOnTerm(term) {
            return simpleModified
        }

        // else use the user provided folder
        guard let op = term.operationType else { return nil }
        let operands = term.operands.lazy.map { $0.constantValue }
        guard let values = sequence(operands) else { return nil }
        return folder.foldConstants(values, withOperation: op).map(Term.Const)
    }
}

private func sequence<T, S: SequenceType where S.Generator.Element == T?>(seq: S) -> [T]? {
    return seq.reduce([]) { acc, elem in
        acc.flatMap { acc in elem.map { acc + [$0] } }
    }
}

public extension SequenceType {
    func reduce1(combine: (Generator.Element, Generator.Element) throws -> Generator.Element) rethrows -> Generator.Element {
        var generator = generate()
        guard let first = generator.next() else { preconditionFailure("reduce1 called on empty sequence") }
        return try GeneratorSequence(generator).reduce(first, combine: combine)
    }
}
