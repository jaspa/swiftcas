//
//  OperationType.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 04.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public enum OperationType: String {
    case Negation = "-"
    case Addition = "+"
    case Multiplication = "*"
}

public extension OperationType {

    var operandCount: Int {
        switch self {
            case .Negation      : return 1
            case .Addition      : return 2
            case .Multiplication: return 2
        }
    }

    var isAssociative: Bool {
        return self == .Addition || self == .Multiplication
    }
}

public extension Term {

    var operationType: OperationType? {
        switch self {
            case .Hole : return nil
            case .Const: return nil
            case .Neg  : return .Negation
            case .Sum  : return .Addition
            case .Prod : return .Multiplication
        }
    }
}
