//
//  ReductorResult.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 12.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public struct ReductorResult<C: Value, H: Variable> {
    public let term: Term<C, H>
    public let modified: Bool

    init(newTerm: Term<C, H>?, @autoclosure fallback: () -> Term<C, H>) {
        term = newTerm ?? fallback()
        modified = newTerm != nil
    }

    init(term: Term<C, H>, modified: Bool = false) {
        self.term = term
        self.modified = modified
    }

    func setModified() -> ReductorResult {
        return ReductorResult(term: term, modified: true)
    }
}
