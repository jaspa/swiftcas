//
//  FunctionReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 06.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// Reduce a Term by a given function.
public struct FunctionReductor<Constant: Value, Hole: Variable>: Reductor {
    public let function: Term<Constant, Hole> -> Term<Constant, Hole>?
    public let wantsLeafs: Bool

    public init(wantsLeafs: Bool = false, function: Term<Constant, Hole> -> Term<Constant, Hole>?) {
        self.function = function
        self.wantsLeafs = wantsLeafs
    }

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        return function(term)
    }
}
