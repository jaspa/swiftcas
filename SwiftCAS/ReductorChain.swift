//
//  ReductorChain.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 05.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// Chain multiple Reductors together. They are applied in a specific order.
public struct ReductorChain<C: Value, H: Variable>: ReductorType {
    private typealias ReduceFunction = Term<C, H> -> ReductorResult<C, H>
    private let function: ReduceFunction

    private init(first: ReduceFunction, second: ReduceFunction, onlyIfModified: Bool) {
        function = { term in
            let firstResult = first(term)
            if firstResult.modified {
                return second(firstResult.term).setModified()
            } else if onlyIfModified {
                return firstResult
            } else {
                return second(firstResult.term)
            }
        }
    }

    public func reduce(term: Term<C, H>) -> ReductorResult<C, H> {
        return function(term)
    }
}

public extension ReductorType {

    /// Chain two reductors together.
    ///
    /// - parameter reductor: The reductor to run after `self`.
    /// - parameter onlyIfModified: Run the given reductor only iff `self` modified the term to reduce.
    func then<R: ReductorType where R.Constant == Constant, R.Hole == Hole>
        (reductor: R, onlyIfModified: Bool = false) -> ReductorChain<Constant, Hole> {
            return ReductorChain(first: reduce, second: reductor.reduce, onlyIfModified: onlyIfModified)
    }
}
