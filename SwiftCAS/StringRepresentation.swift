//
//  StringRepresentation.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 04.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

extension Term: CustomStringConvertible {

    public var description: String {
        var indenter = AutoIndenter(stream: "")
        writeStringRep(&indenter)
        return indenter.stream
    }

    private func writeStringRep<S: OutputStreamType>(inout result: AutoIndenter<S>) -> String? {
        var leafRep: String? = nil

        switch self {
            case let .Hole(h) : leafRep = String(h)
            case let .Const(c): leafRep = String(c)
            case let .Neg(arg)  : return Term.writeStringRep("-", arg: arg, toStream: &result)
            case let .Sum(args) : Term.writeStringRep("+", args: args, toStream: &result)
            case let .Prod(args): Term.writeStringRep("*", args: args, toStream: &result)
        }

        if let leafRep = leafRep { result.write(leafRep) }
        return leafRep
    }

    private func writeShortRep<S: OutputStreamType>(allowOnlyTrueLeafs onlyTrueLeafs: Bool = false, inout toStream result: AutoIndenter<S>) -> String? {
        if onlyTrueLeafs ? isTrueLeaf : isLeaf { return writeStringRep(&result) }
        result.write("_")
        return nil
    }

    static private func writeStringRep<S: OutputStreamType>(op: String, arg: Term, inout toStream result: AutoIndenter<S>) -> String? {
        result.write(op)

        if let shortRep = arg.writeShortRep(allowOnlyTrueLeafs: true, toStream: &result) {
            return op + shortRep
        }

        // `arg` is not a simple leaf
        result.linebreak()
        result.write(" ›  ")
        result.withChildIndent("    ", arg.writeStringRep)
        return nil
    }

    static private func writeStringRep<S: OutputStreamType>(op: String, args: (a: Term, b: Term), inout toStream result: AutoIndenter<S>) {
        let aLeafRep = args.a.writeShortRep(toStream: &result)
        result.write(" \(op) ")
        let bIsLeaf = args.b.writeShortRep(toStream: &result) != nil
        let aIsLeaf = aLeafRep != nil

        if aIsLeaf && bIsLeaf { return }

        result.linebreak()

        if !bIsLeaf {
            let bIndent: String

            if let aLeafRep = aLeafRep {
                let indentSize = aLeafRep.characters.count + op.characters.count + 2
                bIndent = String(count: indentSize, repeatedValue: " " as Character)
            } else {
                let indentSize = op.characters.count + 2
                bIndent = "|" + String(count: indentSize, repeatedValue: " " as Character)
            }

            result.write(bIndent + "›  ")
            result.withChildIndent(bIndent + "   ", args.b.writeStringRep)

        } else {
            result.write("|")
        }

        if !aIsLeaf {
            result.linebreak()
            result.write("›  ")
            result.withChildIndent("   ", args.a.writeStringRep)
        }
    }
}

private struct AutoIndenter<Stream: OutputStreamType>: OutputStreamType {
    var stream: Stream
    let indent: String

    init(stream: Stream, indent: String = "") {
        self.stream = stream
        self.indent = indent
    }

    init(parent: AutoIndenter, newIndent: String) {
        self.stream = parent.stream
        self.indent = parent.indent + newIndent
    }

    mutating func write(string: String) {
        stream.write(string)
    }

    mutating func linebreak() {
        stream.write("\n" + indent)
    }

    mutating func withChildIndent<R>(newIndent: String, _ f: inout AutoIndenter -> R) -> R {
        var child = AutoIndenter(parent: self, newIndent: newIndent)
        let r = f(&child)
        stream = child.stream
        return r
    }
}
