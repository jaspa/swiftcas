//
//  DoubleValues.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 04.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

extension Double: Value {
    public init(doubleValue value: Double) {
        self = value
    }

    public var negated: Double { return -self }
    public func add(value: Double) -> Double { return self + value }
    public func mul(value: Double) -> Double { return self * value }
}

public struct SimpleDoubleFolder: ConstantFolder {
    public func foldConstants(values: [Double], withOperation operation: OperationType) -> Double? {
        return operation.doubleFunction.map { values.reduce1($0) }
    }
}

private extension OperationType {
    var doubleFunction: ((Double, Double) -> Double)? {
        switch self {
            case .Negation      : return nil
            case .Addition      : return (+)
            case .Multiplication: return (*)
        }
    }
}
