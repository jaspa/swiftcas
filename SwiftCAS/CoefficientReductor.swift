//
//  CoefficientReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 08.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// The `CoefficientPreparationReductor` prepares a `Term` for the reduction by `CoefficientReductor`.
///
/// The simple thing this reductor does is to write (nearly) everything in a coefficient-notation. As far as possible.
public struct CoefficientPreparationReductor<Constant: Value, Hole: Variable>: Reductor {

    /// Create a `CoefficientPreparationReductor`.
    public init() {}

    public var wantsLeafs: Bool { return true }

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        switch term {
            case .Const: return .Prod(term, .One)

            case let .Neg(t): return negateCoefficientTerm(t)

            case let .Prod(a, _) where a.isConstant: return nil
            case let .Prod(_, b) where b.isConstant: return term.swappedOperands

            default:
                return .Prod(.One, term)
        }
    }

    private func negateCoefficientTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole> {
        guard case let .Prod(a, b) = term else {
            preconditionFailure("invalid term ** expected coefficient representation")
        }

        return .Prod(a.negated, b)
    }
}

/// Reduce a chain of additions (at some later point multiplications as well) by combining their coefficients.
public struct CoefficientReductor<Constant: Value, Hole: Variable>: Reductor {
    private typealias T = Term<Constant, Hole>

    /// Create a `CoefficientReductor`.
    public init() {}

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        if term.operationType != .Addition {
            return nil
        }

        let summands = term.associativeOperands

        if summands.count < 2 {
            return nil
        }

        var summandsMap = [T: Constant]()

        for sum in summands {
            if summandsMap[sum]?++ == nil {
                summandsMap[sum] = Constant(doubleValue: 1)
            }
        }

        return summandsMap.lazy.filter { !$1.isZero }.map { T.Prod(.Const($1), $0) }.reduce1(T.Sum)
    }

    public static var chain: ReductorChain<Constant, Hole> {
        return CoefficientPreparationReductor().then(CoefficientReductor())
    }
}
