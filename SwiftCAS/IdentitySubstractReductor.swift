//
//  IdentitySubstractReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 05.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// IdentitySubtractReductor eliminates cases where a value is subtracted from itself.
///
///     ( a + -a) ==> 0
///     (-a +  a) ==> 0
public struct IdentitySubstractReductor<Constant: Value, Hole: Variable>: Reductor {

    public init() {}

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        if case let .Sum(a, .Neg(b)) = term where a == b {
            return .Zero
        } else if case let .Sum(.Neg(a), b) = term where a == b {
            return .Zero
        }

        return nil
    }
}
