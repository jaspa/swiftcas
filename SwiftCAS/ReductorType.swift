//
//  ReductorType.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 12.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public protocol ReductorType {
    typealias Constant: Value
    typealias Hole: Variable

    func reduce(term: Term<Constant, Hole>) -> ReductorResult<Constant, Hole>
}

public extension ReductorType {
    func reduceToTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole> {
        return reduce(term).term
    }
}
