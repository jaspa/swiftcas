//
//  TermModification.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 04.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public extension Term {

    var swappedOperands: Term {
        var new = self
        new.operands = new.operands.leftShifted
        return new
    }

    var negated: Term {
        if case let .Neg(t) = self {
            return t
        } else {
            return .Neg(self)
        }
    }
}

private extension RangeReplaceableCollectionType {

    var leftShifted: Self {
        if self.count <= 1 { return self }

        var new = self
        let elem = new.removeFirst()
        new.append(elem)
        return new
    }
}
