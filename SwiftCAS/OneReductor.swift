//
//  OneReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 06.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// `OneReductor` handles cases where something is multiplied by one or minus one.
public struct OneReductor<Constant: Value, Hole: Variable>: Reductor {

    public init() {}

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        switch term {
            case let .Prod(.Const(c), t) where c.isOne: return t
            case let .Prod(.Neg(.Const(c)), t) where c.isOne: return t.negated

            case let .Prod(t, .Const(c)) where c.isOne: return t
            case let .Prod(t, .Neg(.Const(c))) where c.isOne: return t.negated

            default: return nil
        }
    }
}
