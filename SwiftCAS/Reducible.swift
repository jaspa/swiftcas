//
//  Reducible.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 12.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public protocol Reducible {
    typealias Constant: Value
    // Hole is already used by Term
    typealias HoleType: Variable

    func reduce<R: ReductorType where R.Constant == Constant, R.Hole == HoleType>(reductor: R) -> ReductorResult<Constant, HoleType>
}

public extension Reducible {
    func reduce<R: ReductorType where R.Constant == Constant, R.Hole == HoleType>(reductor: R) -> Term<Constant, HoleType> {
        return reduce(reductor).term
    }
}

extension Term: Reducible {
    public func reduce<R: ReductorType where R.Constant == C, R.Hole == H>(reductor: R) -> ReductorResult<C, H> {
        return reductor.reduce(self)
    }
}

extension ReductorResult: Reducible {
    public func reduce<R: ReductorType where R.Constant == C, R.Hole == H>(reductor: R) -> ReductorResult<C, H> {
        return reductor.reduce(term)
    }

    public func reduceIfModified<R: ReductorType where R.Constant == C, R.Hole == H>(reductor: R) -> ReductorResult<C, H> {
        return modified ? reductor.reduce(term) : self
    }
}
