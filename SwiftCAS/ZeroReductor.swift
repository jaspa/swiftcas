//
//  ZeroReductor.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

/// `ZeroReductor` simplfies terms containing zeros.
///
///     (a + 0) ==> a
///     (a * 0) ==> 0
public struct ZeroReductor<Constant: Value, Hole: Variable>: Reductor {

    //  Required by Xcode; else no constructor is exported to the public
    /// Default constructor for ZeroReductor
    public init() {}

    public func runOnTerm(term: Term<Constant, Hole>) -> Term<Constant, Hole>? {
        switch term {
            case let .Sum(a, b) where a.isZero: return b
            case let .Sum(a, b) where b.isZero: return a

            case let .Prod(a, b) where a.isZero || b.isZero: return .Zero

            default: return nil
        }
    }
}
