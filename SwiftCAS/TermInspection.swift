//
//  TermInspection.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

public extension Term {
    var isTrueLeaf: Bool { return operationType == nil }

    var isLeaf: Bool {
        if isTrueLeaf { return true }
        if case let .Neg(t) = self { return t.isTrueLeaf }
        return false
    }

    var isConstant: Bool {
        if case .Const = self { return true }
        return false
    }

    var isZero: Bool {
        guard case let .Const(c) = self else { return false }
        return c.isZero
    }

    var operands: [Term] {
        get {
            switch self {
                case .Hole : return []
                case .Const: return []
                case let .Neg(a)    : return [a]
                case let .Sum(a, b) : return [a, b]
                case let .Prod(a, b): return [a, b]
            }
        }
        set(operands) {
            let buildMsg: () -> String = {
                guard let type = self.operationType else { return "cannot set operands for leaves" }
                return "invalid number of operands for (\(type.rawValue)) - expected \(type.operandCount), given \(operands.count)"
            }
            assert(operands.count == operationType?.operandCount ?? 0, buildMsg())

            switch self {
                // Nothing to do for leaves
                case .Hole : break
                case .Const: break

                case .Neg : self = .Neg(operands[0])
                case .Sum : self = .Sum(operands[0], operands[1])
                case .Prod: self = .Prod(operands[0], operands[1])
            }
        }
    }

    /// If the same associative operation is chained with itself, return all the operands.
    var associativeOperands: [Term] {
        guard let type = operationType where type.isAssociative else { return [] }
        return associativeOperandsOfType(type)
    }

    private func associativeOperandsOfType(type: OperationType) -> [Term] {
        return operands.flatMap { (operand) -> [Term] in
            if (operand.operationType.map { $0 == type } ?? false) {
                return operand.associativeOperandsOfType(type)
            } else {
                return [operand]
            }
        }
    }

    var constantValue: C? {
        if case let .Const(c) = self {
            return c
        } else {
            return nil
        }
    }
}
