//
//  ReductorResultTests.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 12.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

import XCTest
import SwiftCAS

class ReductorResultTests: XCTestCase {

    private struct ReductorSpec: OptionSetType {
        let rawValue: UInt8
        init(rawValue: UInt8) { self.rawValue = rawValue }

        static let Modifying = ReductorSpec(rawValue: 0b001)
        static let IsR2      = ReductorSpec(rawValue: 0b010)
        static let R2HasRun  = ReductorSpec(rawValue: 0b100)
    }

    private func getReductor(specary: ReductorSpec..., file: String = __FILE__, line: UInt = __LINE__) -> R {
        let spec = ReductorSpec(specary)
        return FunctionReductor(wantsLeafs: true) { term in
            if spec.contains(.R2HasRun) {
                XCTAssertTrue(self.reductor2Run, "'reductor2' should have been run", file: file, line: line)
            } else {
                XCTAssertFalse(self.reductor2Run, "'reductor2' shouldn't have been run", file: file, line: line)
            }

            if spec.contains(.IsR2) {
                self.reductor2Run = true
            }

            return spec.contains(.Modifying) ? term : nil
        }
    }

    private func CheckReductor2Run(run: Bool = true, file: String = __FILE__, line: UInt = __LINE__) {
        if run {
            XCTAssertTrue(reductor2Run, "'reductor2' should be run by now", file: file, line: line)
        } else {
            XCTAssertFalse(reductor2Run, "'reductor2' shouldn't be run", file: file, line: line)
        }
    }

    private var reductor2Run = false

    override func setUp() {
        super.setUp()
        reductor2Run = false
    }

    func testModifiedDoesNotCarryTrough() {
        let r1 = getReductor(.Modifying)
        let r2 = getReductor(.IsR2)
        let result = T.Zero.reduce(r1).reduce(r2)
        XCTAssertFalse(result.modified, ".modified should not carry through without a chain")
        CheckReductor2Run()
    }

    func testModifiedCarriesTroughWithChain() {
        let r1 = getReductor(.Modifying)
        let r2 = getReductor(.IsR2)
        let result = T.Zero.reduce(r1.then(r2))
        XCTAssertTrue(result.modified, ".modified should carry through when using a chain")
        CheckReductor2Run()
    }

    func testReducingOnlyIfModified() {
        let r1 = getReductor()
        let r2 = getReductor(.IsR2)
        let result = T.Zero.reduce(r1.then(r2, onlyIfModified: true))
        XCTAssertFalse(result.modified, "the term should have never been modified")
        CheckReductor2Run(false)
    }
}
