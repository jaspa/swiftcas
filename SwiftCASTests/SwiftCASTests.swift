//
//  SwiftCASTests.swift
//  SwiftCASTests
//
//  Created by Janek Spaderna on 03.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

import XCTest
import SwiftCAS

private func AssertStringsEqual(@autoclosure lhs: () -> String, @autoclosure _ rhs: () -> String, _ message: String = "", file: String = __FILE__, line: Int = __LINE__) {
    let a = lhs(), b = rhs()

    if a != b {
        print("\n", a, "\n\n\n", b, "\n")
        XCTFail(message, file: file, line: UInt(line))
    }
}

class SwiftCASTests: XCTestCase {

    func testStringRepresentation() {
        let term: T =
            .Prod(
                .Sum(
                    .Prod(
                        .Hole("a"), .Const(3)
                    ),
                    .Prod(
                        .Const(7),
                        .Neg(
                            .Sum(
                                .Const(1), .Neg(.Const(1))
                            )
                        )
                    )
                ),
                .Const(2)
        )

        let expected = [
            "_ * 2.0",
            "|",
            "›  _ + _",
            "   |   ›  7.0 * _",
            "   |            ›  -_",
            "   |                ›  1.0 + -1.0",
            "   ›  a * 3.0"
        ].joinWithSeparator("\n")

        AssertStringsEqual(String(term), expected, "invalid string representation")
    }

    func testMulByZeroCascades() {
        // given:    (0*2)*a
        // expected:  0

        let term: T = .Prod(.Prod(.Const(0), .Const(2)), .Hole("a"))

        AssertTermsEqual(term.reduce(ZeroReductor()), .Zero)
        AssertTermsEqual(term.swappedOperands.reduce(ZeroReductor()), .Zero)
    }

    func testPlusZeroIsEliminated() {
        // given:    1+0
        // expected: 1

        let term: T = .Sum(.Const(1), .Const(0))

        AssertTermsEqual(term.reduce(ZeroReductor()), .Const(1))
        AssertTermsEqual(term.swappedOperands.reduce(ZeroReductor()), .Const(1))
    }

    func testMulByOneIsEliminated() {
        let term: T = .Prod(.Hole("a"), .Const(1))
        AssertTermsEqual(term.reduce(OneReductor()), .Hole("a"))
        AssertTermsEqual(term.swappedOperands.reduce(OneReductor()), .Hole("a"))
    }

    func testMulByMinusOneNegates() {
        let term: T = .Prod(.Hole("a"), .Neg(.Const(1)))
        AssertTermsEqual(term.reduce(OneReductor()), .Neg(.Hole("a")))
        AssertTermsEqual(term.swappedOperands.reduce(OneReductor()), .Neg(.Hole("a")))
    }

    func testIdentitySubtractionIsEliminatedWithConstants() {
        let term: T = .Sum(.Const(1), .Neg(.Const(1)))
        AssertTermsEqual(term.reduce(IdentitySubstractReductor()), .Zero)
        AssertTermsEqual(term.swappedOperands.reduce(IdentitySubstractReductor()), .Zero)
    }

    func testIdentitySubtractionIsEliminatedWithHoles() {
        let term: T = .Sum(.Hole("a"), .Neg(.Hole("a")))
        AssertTermsEqual(term.reduce(IdentitySubstractReductor()), .Zero)
        AssertTermsEqual(term.swappedOperands.reduce(IdentitySubstractReductor()), .Zero)
    }

    func testIdentitySubtractionIsEliminatedWithComplexBranches() {
        let term: T = .Sum(.Prod(.Const(2), .Hole("a")), .Neg(.Prod(.Hole("a"), .Const(2))))
        AssertTermsEqual(term.reduce(IdentitySubstractReductor()), .Zero)
        AssertTermsEqual(term.swappedOperands.reduce(IdentitySubstractReductor()), .Zero)
    }

    func testNegationReductorWorksInBothDirections() {
        let term: T = .Neg(.Const(3))
        let reduced: T = term.reduce(NegationReductor(mode: .Reducing))
        
        XCTAssertEqual(reduced.constantValue, -3)
        AssertTermsEqual(reduced.reduce(NegationReductor(mode: .Decomposing)), term)
    }

    func testDoubleNegationsAreRemoved() {
        let term: T = .Neg(.Neg(.Const(3)))
        let reduced: T = term.reduce(NegationReductor(mode: .Reducing))
        let decomposed: T = term.reduce(NegationReductor(mode: .Decomposing))
        
        XCTAssertEqual(reduced.constantValue, 3, "double negations not removed while .Reducing")
        XCTAssertEqual(decomposed.constantValue, 3, "double negations not removed while .Decomposing")
    }

    func testCoefficientPreparationReductor() {
        // -a         => -1 * a
        // 3         => 3 * 1
        // 1*a + 3*1 => 1 * (1*a + 3*1)

        let term: T = .Sum(.Neg(.Hole("a")), .Const(3))
        let expected: T = .Prod(.One, .Sum(.Prod(.Neg(.One), .Hole("a")), .Prod(.Const(3), .One)))

        AssertTermsEqual(term.reduce(CoefficientPreparationReductor()), expected, relaxOrdering: false)
    }

    func testSomeSimpleReductorChain() {
        let term: T = .Sum(.Sum(.Neg(.Const(3)), .Const(0)), .Const(10))
        let chain: C = ZeroReductor().then(NegationReductor(mode: .Reducing)).then(SimpleConstantReductor())
        AssertTermsEqual(term.reduce(chain), .Const(7))
    }
}
