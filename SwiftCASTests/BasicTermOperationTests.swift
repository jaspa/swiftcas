//
//  BasicTermInspectionTests.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 13.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

import XCTest
import SwiftCAS

class BasicTermInspectionTests: XCTestCase {

    // MARK: - inspecting operations

    func testAssociativeOperands() {
        let term: T = .Sum(.Sum(.Const(2), .Hole("a")), .Sum(.Sum(.Const(1), .Neg(.Const(1))), .Hole("b")))
        XCTAssertEqual(term.associativeOperands, [.Const(2), .Hole("a"), .Const(1), .Neg(.Const(1)), .Hole("b")])
    }

    func testSwappedOperands() {
        AssertTermsEqual(T.Zero, .Zero, relaxOrdering: false)
        AssertTermsEqual(T.Sum(.Const(1), .Hole("a")).swappedOperands, .Sum(.Hole("a"), .Const(1)),
            relaxOrdering: false)
        AssertTermsEqual(T.Prod(.Const(1), .Hole("a")).swappedOperands, .Prod(.Hole("a"), .Const(1)),
            relaxOrdering: false)
    }

    // MARK: - modifying operations

    func testNegatedEliminatesDoubleNegations() {
        AssertTermsEqual(T.Neg(.Const(1)).negated, .Const(1))
    }
}
