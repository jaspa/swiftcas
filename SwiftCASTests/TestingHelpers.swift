//
//  TestingHelpers.swift
//  SwiftCAS
//
//  Created by Janek Spaderna on 13.09.15.
//  Copyright © 2015 jds. All rights reserved.
//

import XCTest
import SwiftCAS

typealias T  = Term<Double, String>
typealias C  = ReductorChain<Double, String>
typealias R  = FunctionReductor<Double, String>
typealias RR = ReductorResult<Double, String>

func AssertTermsEqual<C: Value, H: Variable>(
    @autoclosure lhs: () -> Term<C, H>, @autoclosure _ rhs: () -> Term<C, H>, relaxOrdering: Bool = true,
    file: String = __FILE__, line: UInt = __LINE__) {

        let a = lhs(), b = rhs()

        XCTAssertTrue(
            a.isEqualToTerm(b, relaxOrdering: relaxOrdering),
            "expected term\n\(a)\nto be equal to term\n\(b)",
            file: file, line: line)
}
